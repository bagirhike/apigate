package com.ddtekno.gateway.model;

import java.util.Map;

public class Gateway {
	
	private String id;
	private String fsp;
	private Gateway refund;
	private String service;
	private String token;
	private Map<String,Object> props;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Map<String,Object> getProps() {
		return props;
	}
	public void setProps(Map<String,Object> props) {
		this.props = props;
	}
	public String getFsp() {
		return fsp;
	}
	public void setFsp(String fsp) {
		this.fsp = fsp;
	}
	public Gateway getRefund() {
		return refund;
	}
	public void setRefund(Gateway refund) {
		this.refund = refund;
	}
	@Override
	public String toString() {
		return "Gateway [id=" + id + ", fsp=" + fsp + ", service=" + service + ", token=" + token + ", props=" + props
				+ "]";
	}

}
