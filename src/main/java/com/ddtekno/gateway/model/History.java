package com.ddtekno.gateway.model;

import java.util.Map;

public class History {
	
	private String channelId;
	private String timestamp;
	private String service;
	private String fspId;
	private String code;
	private Map<String,Object> message;
	
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getFspId() {
		return fspId;
	}
	public void setFspId(String fspId) {
		this.fspId = fspId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Map<String,Object> getMessage() {
		return message;
	}
	public void setMessage(Map<String,Object> message) {
		this.message = message;
	}

}
