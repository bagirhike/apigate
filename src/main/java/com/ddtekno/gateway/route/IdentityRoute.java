package com.ddtekno.gateway.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class IdentityRoute extends RouteBuilder {
	
	@Value("${gateway.identity.host}") 
	private String identity;

	@Override
	public void configure() throws Exception {

		from("direct:addChannel").id("gateway.addchannel")
		.setHeader(Exchange.CONTENT_TYPE).constant("application/json")
		.setHeader(Exchange.HTTP_METHOD).constant("POST")
		.setBody().body(String.class)
		.to("http://"+identity+":8012/channel/add")
		.convertBodyTo(String.class).end();
		
		from("direct:updateChannel").id("gateway.upchannel")
		.setHeader(Exchange.CONTENT_TYPE).constant("application/json")
		.setHeader(Exchange.HTTP_METHOD).constant("POST")
		.setBody().body(String.class)
		.to("http://"+identity+":8012/channel/update")
		.convertBodyTo(String.class).end();
		
		from("direct:getChannel").id("gateway.getchannel")
		.toD("http://"+identity+":8012/channel/get?id=${body}")
		.convertBodyTo(String.class).end();
	}
}