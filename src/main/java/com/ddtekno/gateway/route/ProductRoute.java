package com.ddtekno.gateway.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ddtekno.gateway.model.Gateway;
import com.ddtekno.gateway.processor.ProductProcessor;

@Component
public class ProductRoute extends RouteBuilder {
	
	@Value("${gateway.identity.host}") 
	private String identity;

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Gateway.class);

		from("direct:getProduct").id("getproduct").marshal(jsonDataFormat)
		.process(new ProductProcessor(ProductProcessor.GET_CHANNEL))
		.toD("http://"+identity+":8012/channel/get?id=${header.chanId}")
		.process(new ProductProcessor(ProductProcessor.GET_ORG))
		.choice()
			.when().simple("${header.error}")
				.convertBodyTo(String.class)
			.otherwise()
				.toD("http://"+identity+":8012/org/getproduct?id=${header.orgId}&prodId=${header.prodId}")
				.convertBodyTo(String.class).end();
		
		from("direct:getProducts").id("getproducts").marshal(jsonDataFormat)
		.process(new ProductProcessor(ProductProcessor.GET_CHANNEL))
		.toD("http://"+identity+":8012/channel/get?id=${header.chanId}")
		.process(new ProductProcessor(ProductProcessor.GET_ORG))
		.choice()
			.when().simple("${header.error}")
				.convertBodyTo(String.class)
			.otherwise()
				.toD("http://"+identity+":8012/org/getproducts?id=${header.orgId}&prodType=${header.prodType}")
				.convertBodyTo(String.class).end();
	}
}