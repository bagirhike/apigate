package com.ddtekno.gateway.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HistoryRoute extends RouteBuilder {
	
	@Value("${gateway.fspadapter.host}") 
	private String fspadapter;

	@Override
	public void configure() throws Exception {
		
		from("direct:getHistory").id("gateway.gethistory")
		.toD("http://"+fspadapter+":8011/fsp/history?channelId=${body}")
		.convertBodyTo(String.class).end();
	}
}