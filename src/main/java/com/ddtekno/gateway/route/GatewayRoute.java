package com.ddtekno.gateway.route;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ddtekno.gateway.model.Gateway;
import com.ddtekno.gateway.processor.GatewayProcessor;

@Component
public class GatewayRoute extends RouteBuilder {
	
	@Value("${gateway.identity.host}") 
	private String identity;
	@Value("${gateway.fspadapter.host}") 
	private String fsp;
	@Value("${gateway.fspadapter.port}") 
	private String fspPort;

	@Override
	public void configure() throws Exception {

		JacksonDataFormat jsonDataFormat = new JacksonDataFormat(Gateway.class);

		from("direct:startGateway").id("gateway").marshal(jsonDataFormat)
		.process(new GatewayProcessor(GatewayProcessor.GET_CHANNEL))
		.toD("http://"+identity+":8012/channel/get?id=${header.chanId}")
		.process(new GatewayProcessor(GatewayProcessor.GET_ORG))
		.choice()
			.when().simple("${header.error}")
				.convertBodyTo(String.class)
			.otherwise()
				.toD("http://"+identity+":8012/org/get?id=${header.orgId}&fspId=${header.fspId}&prodId=${header.prodId}")
				.process(new GatewayProcessor(GatewayProcessor.GET_FSP))
				.choice()
					.when().simple("${header.error}")
						.convertBodyTo(String.class)
					.otherwise()
						.toD("http://"+fsp+":"+fspPort+"/fsp/call")
						.convertBodyTo(String.class).end();
	}
}