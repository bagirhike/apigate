package com.ddtekno.gateway.controller;

import java.util.concurrent.ExecutionException;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HistoryController {
	
	@Produce(uri = "direct:getHistory")
	private ProducerTemplate getHistory;
	
	@RequestMapping(value = "/gateway/history", method = RequestMethod.GET)
	@ResponseBody
	public String gatewayGetChannel(@RequestParam String user) throws InterruptedException, ExecutionException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object msg = getHistory.requestBody(getHistory.getDefaultEndpoint(), user);
		return msg.toString();
	}
	
	
}
