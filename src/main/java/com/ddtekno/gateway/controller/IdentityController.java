package com.ddtekno.gateway.controller;

import java.util.concurrent.ExecutionException;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IdentityController {
	
	@Produce(uri = "direct:addChannel")
	private ProducerTemplate addChannel;
	
	@Produce(uri = "direct:updateChannel")
	private ProducerTemplate updateChannel;
	
	@Produce(uri = "direct:getChannel")
	private ProducerTemplate getChannel;
	
	@RequestMapping(value = "/gateway/addchannel", method = RequestMethod.POST)
	@ResponseBody
	public String gatewayAddChannel(@RequestBody String body) throws InterruptedException, ExecutionException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object msg = addChannel.requestBody(addChannel.getDefaultEndpoint(), body);
		return msg.toString();
	}
	
	@RequestMapping(value = "/gateway/updatechannel", method = RequestMethod.POST)
	@ResponseBody
	public String gatewayUpdateChannel(@RequestBody String body) throws InterruptedException, ExecutionException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object msg = updateChannel.requestBody(updateChannel.getDefaultEndpoint(), body);
		return msg.toString();
	}
	
	@RequestMapping(value = "/gateway/getchannel", method = RequestMethod.GET)
	@ResponseBody
	public String gatewayGetChannel(@RequestParam String id) throws InterruptedException, ExecutionException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object msg = getChannel.requestBody(getChannel.getDefaultEndpoint(), id);
		return msg.toString();
	}
	
	
}
