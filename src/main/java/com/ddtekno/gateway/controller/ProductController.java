package com.ddtekno.gateway.controller;

import java.util.concurrent.ExecutionException;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ddtekno.gateway.model.Gateway;

@RestController
public class ProductController {
	
	@Produce(uri = "direct:getProduct")
	private ProducerTemplate getProduct;
	
	@Produce(uri = "direct:getProducts")
	private ProducerTemplate getProducts;
	
	@RequestMapping(value = "/gateway/product", method = RequestMethod.POST)
	@ResponseBody
	public String gatewayGetProduct(@RequestBody Gateway conf) throws InterruptedException, ExecutionException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object msg = getProduct.requestBody(getProduct.getDefaultEndpoint(), conf);
		return msg.toString();
	}
	
	@RequestMapping(value = "/gateway/products", method = RequestMethod.POST)
	@ResponseBody
	public String gatewayGetProducts(@RequestBody Gateway conf) throws InterruptedException, ExecutionException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object msg = getProducts.requestBody(getProducts.getDefaultEndpoint(), conf);
		return msg.toString();
	}
	
	
}
