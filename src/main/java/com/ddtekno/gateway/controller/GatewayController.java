package com.ddtekno.gateway.controller;

import java.util.concurrent.ExecutionException;

import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ddtekno.gateway.model.Gateway;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class GatewayController {

	@Produce(uri = "direct:startGateway")
	private ProducerTemplate gateway;

	private final ObjectMapper obj = new ObjectMapper();

	@RequestMapping(value = "/gateway", method = RequestMethod.POST)
	@ResponseBody
	public String request(@RequestBody Gateway conf) throws InterruptedException, ExecutionException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object msg = gateway.requestBody(gateway.getDefaultEndpoint(), conf);
		String result = msg.toString();
		try {
			result = checkNextActions(conf, result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(result);
			e.printStackTrace();
			return "{\"message\":\"Transaksi Gagal\"}";
		}

		return result;
	}

	private String checkNextActions(Gateway conf, String result) throws Exception {
		// next hop action
			JSONObject json = new JSONObject(result);
			if (json.has("action")) {
				String action = json.getString("action");
				json.remove("action");
				if (action.equals("buy") || action.equals("refund")) {
					if (json.has("refund") && !json.isNull("refund")) {
						try {
							String refund = json.getJSONObject("refund").toString();
							Gateway r = obj.readValue(refund, Gateway.class);
							conf.setRefund(r);
						} catch (Exception err) {
							err.printStackTrace();
						}
					}
					conf.setId(json.getString("id"));
					conf.setFsp(json.getString("fsp"));
					conf.setService(json.getString("service"));
					conf.setProps(json.getJSONObject("props").toMap());
					Object msg = gateway.requestBody(gateway.getDefaultEndpoint(), conf);
					return checkNextActions(conf, msg.toString());
				}

			}
			
			return result;

	}

}
