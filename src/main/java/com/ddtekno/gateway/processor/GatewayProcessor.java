package com.ddtekno.gateway.processor;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.json.JSONObject;

import com.ddtekno.gateway.model.Gateway;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GatewayProcessor implements Processor {

	public static final int GET_CHANNEL = 0;
	public static final int GET_ORG = 1;
	public static final int GET_FSP = 2;
	private int type;
	private ObjectMapper obj;
	private JSONObject json;
	private Map<String, Object> map;

	public GatewayProcessor(int type) {
		this.type = type;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		obj = new ObjectMapper();
		switch (type) {
		case GET_CHANNEL:
			Gateway conf = obj.readValue(exchange.getIn().getBody(String.class), Gateway.class);
			exchange.getOut().setHeader("chanId", conf.getId());
			exchange.getOut().setHeader("conf", conf);
			// exchange.getOut().setHeader("fspId", conf.getFsp());
			// exchange.getOut().setHeader("service", conf.getService());
			// exchange.getOut().setHeader("token", conf.getToken());
			// exchange.getOut().setHeader("props", conf.getProps());
			break;
		case GET_ORG:
			json = new JSONObject(exchange.getIn().getBody(String.class));
			if (!json.isNull("message")) {
				exchange.getOut().setHeader("error", true);
				exchange.getOut().setBody(json.toString());
			} else {
				conf = exchange.getIn().getHeader("conf", Gateway.class);

				if (conf.getProps() != null && conf.getProps().containsKey("prodId"))
					exchange.getOut().setHeader("prodId", conf.getProps().get("prodId"));
				else
					exchange.getOut().setHeader("prodId", "none");

				exchange.getOut().setHeader("orgId", json.query("/auth/orgId").toString());
				exchange.getOut().setHeader("fspId", conf.getFsp());
				exchange.getOut().setHeader("conf", exchange.getIn().getHeader("conf"));
			}
			break;
		case GET_FSP:
			json = new JSONObject(exchange.getIn().getBody(String.class));
			System.out.println(json.toString());
			if (!json.isNull("message")) {
				exchange.getOut().setHeader("error", true);
				exchange.getOut().setBody(json.toString());
			} else {
				conf = exchange.getIn().getHeader("conf", Gateway.class);
				JSONObject fsp = json.getJSONObject("onFSP");
				map = new HashMap<String, Object>();
				map.put("id", conf.getFsp());
				map.put("service", conf.getService());
				map.put("orgId", json.getString("id"));
				Map<String, Object> auth = fsp.getJSONObject("auth").toMap();
				auth.put("chanId", conf.getId());
				// auth.put("chanName", exchange.getIn().getHeader("chanName"));
				auth.put("token", conf.getToken());
				if (!json.isNull("onProduct")) {
					JSONObject prod = json.getJSONObject("onProduct");
					conf.getProps().put("prodName", prod.getString("name"));
					conf.getProps().put("prodAdmin", prod.getInt("admin"));
					conf.getProps().put("prodPrice", prod.getInt("price"));
					conf.getProps().put("prodType", prod.getString("type"));

					if (prod.has("fsp") && !prod.isNull("fsp")) 
						conf.getProps().put("prodFSP", prod.getString("fsp"));
				}
				
				auth.put("props", conf.getProps());
				if(conf.getRefund()!=null)
					auth.put("refund", conf.getRefund());
				
				if(json.has("settlement") && !json.isNull("settlement"))
					auth.put("settlement",json.getJSONObject("settlement").toMap());
				
				map.put("auth", auth);
				
				exchange.getOut().setBody(obj.writeValueAsString(map), String.class);
				exchange.getOut().setHeader(Exchange.HTTP_METHOD, "POST");
				exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
				break;
			}
			break;
		default:
			exchange.getOut().setHeader("message", "Route Type not Found");
			break;
		}
	}

}
