package com.ddtekno.gateway.processor;

import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.json.JSONObject;

import com.ddtekno.gateway.model.Gateway;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProductProcessor implements Processor {

	public static final int GET_CHANNEL = 0;
	public static final int GET_ORG = 1;
	private int type;
	private ObjectMapper obj;
	private JSONObject json;
	private Map<String, Object> map;

	public ProductProcessor(int type) {
		this.type = type;
	}

	@Override
	public void process(Exchange exchange) throws Exception {
		obj = new ObjectMapper();
		switch (type) {
		case GET_CHANNEL:
			Gateway conf = obj.readValue(exchange.getIn().getBody(String.class), Gateway.class);
			exchange.getOut().setHeader("chanId", conf.getId());
			exchange.getOut().setHeader("conf", conf);
			// exchange.getOut().setHeader("fspId", conf.getFsp());
			// exchange.getOut().setHeader("service", conf.getService());
			// exchange.getOut().setHeader("token", conf.getToken());
			// exchange.getOut().setHeader("props", conf.getProps());
			break;
		case GET_ORG:
			json = new JSONObject(exchange.getIn().getBody(String.class));
			if (!json.isNull("message")) {
				exchange.getOut().setHeader("error", true);
				exchange.getOut().setBody(json.toString());
			} else {
				conf = exchange.getIn().getHeader("conf", Gateway.class);
				
				if (conf.getProps()!=null && conf.getProps().containsKey("prodId"))
					exchange.getOut().setHeader("prodId", conf.getProps().get("prodId"));
				else if(conf.getProps()!=null && conf.getProps().containsKey("prodType"))
					exchange.getOut().setHeader("prodType", conf.getProps().get("prodType"));
				else
					exchange.getOut().setHeader("prodId", "none");

				exchange.getOut().setHeader("orgId", json.query("/auth/orgId").toString());
			}
			break;
		default:
			exchange.getOut().setHeader("message", "Route Type not Found");
			break;
		}
	}

}
